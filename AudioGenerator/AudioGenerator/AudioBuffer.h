#pragma once
#include "SDL.h"
#include <cstring>


class AudioBuffer
{

public:

	AudioBuffer()
	{
		device = 0;

		buffer = new Sint16[8192];
		buffer_size = 0;
		sample_starting_index = 0;
		sample_rate = 0;  // samples per second
	}

	~AudioBuffer()
	{
		delete[] buffer;
		SDL_CloseAudioDevice(device);
		SDL_QuitSubSystem(SDL_INIT_AUDIO);
	}


	SDL_AudioDeviceID Init(int frequency, int samplesInBuffer, SDL_AudioCallback audioCallback)
	{
		SDL_AudioSpec want, have;

		SDL_memset(&want, 0, sizeof(want));
		want.freq = frequency;
		want.format = AUDIO_S16;
		want.channels = 2;  // Stereo
		want.samples = samplesInBuffer;
		want.callback = audioCallback;
		want.userdata = (void*)this;

		delete[] buffer;
		buffer_size = want.channels * want.samples;
		buffer = new Sint16[buffer_size];
		sample_rate = want.freq;
		audioMutex = SDL_CreateMutex();
		Clear();

		SDL_InitSubSystem(SDL_INIT_AUDIO);
		device = SDL_OpenAudioDevice(NULL, 0, &want, &have, 0);
		if (device == 0)
		{
			SDL_Log("Failed to open audio device: %s", SDL_GetError());
			return 0;
		}

		// Unpause (0) to start audio callback
		SDL_PauseAudioDevice(device, 0);
		return device;
	}

	void Clear()
	{
		memset(buffer, 0, buffer_size * sizeof(Sint16));
	}

	void Step()
	{
		sample_starting_index += buffer_size / 2;
	}

	void Lock()
	{
		SDL_LockMutex(audioMutex);
	}

	void Unlock()
	{
		SDL_UnlockMutex(audioMutex);
	}

	int CopyIntoStream(Uint8* stream)
	{
		Sint16* stream16 = (Sint16*)stream;
		memcpy(stream16, buffer, buffer_size * sizeof(Sint16));
		return buffer_size * 2;  // Return the number of bytes (Uint8) we have copied into the stream
	}

	int GetSampleRate()
	{
		return sample_rate;
	}

	int GetSampleStartingIndex()
	{
		return sample_starting_index;
	}

	int GetSize()
	{
		return buffer_size;
	}

	void MixMonoSample(int sampleIndex, Sint16 sample)
	{
		// Double the mono sample to fill both stereo channels
		int index = (sampleIndex - sample_starting_index) * 2;
		if (index < 0 || index >= buffer_size)
			return;

		buffer[index] += sample;
		buffer[index + 1] += sample;
	}

	Sint16 GetSample(int index)
	{
		if (index < 0 || index >= buffer_size)
			return 0;
		return buffer[index];
	}

private:
	SDL_AudioDeviceID device;

	Sint16* buffer;
	int buffer_size;
	int sample_starting_index;
	int sample_rate;  // samples per second

	SDL_mutex* audioMutex = nullptr;
};