﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
public class D20 : MonoBehaviour
{
    public GameObject sphere;

    // Start is called before the first frame update
    void Start()
    {
        Mesh mesh = new Mesh();

        MeshFilter mf = GetComponent<MeshFilter>();
        mf.mesh = mesh;


        List<Vector3> vertices = new List<Vector3>();
        List<Vector3> verticesDuped = new List<Vector3>();

        float phi = (1 + Mathf.Sqrt(5)) / 2;

        vertices.Add(new Vector3(0, 1, phi));  // 0
        vertices.Add(new Vector3(phi, 0, 1));  // 1
        vertices.Add(new Vector3(1, phi, 0));  // 2
        vertices.Add(new Vector3(0, -1, phi)); // 3
        vertices.Add(new Vector3(phi, 0, -1)); // 4
        vertices.Add(new Vector3(-1, phi, 0)); // 5
        vertices.Add(new Vector3(0, 1, -phi)); // 6
        vertices.Add(new Vector3(-phi, 0, 1)); // 7
        vertices.Add(new Vector3(1, -phi, 0)); // 8
        vertices.Add(new Vector3(0, -1, -phi));// 9
        vertices.Add(new Vector3(-phi, 0, -1));// 10
        vertices.Add(new Vector3(-1, -phi, 0));// 11


        for (int i = 0; i < 12; ++i)
        {
            GameObject go = Instantiate(sphere, vertices[i], Quaternion.identity);
            IntHolder h = go.AddComponent<IntHolder>();
            h.value = i;
        }

        List<int> indices = new List<int>();

        indices.Add(0);
        indices.Add(5);
        indices.Add(7);

        indices.Add(5);
        indices.Add(10);
        indices.Add(7);

        indices.Add(7);
        indices.Add(10);
        indices.Add(11);

        indices.Add(10);
        indices.Add(9);
        indices.Add(11);

        indices.Add(11);
        indices.Add(9);
        indices.Add(8);

        indices.Add(9);
        indices.Add(4);
        indices.Add(8);

        indices.Add(4);
        indices.Add(9);
        indices.Add(6);

        indices.Add(3);
        indices.Add(0);
        indices.Add(7);

        indices.Add(7);
        indices.Add(11);
        indices.Add(3);

        indices.Add(9);
        indices.Add(10);
        indices.Add(6);

        indices.Add(6);
        indices.Add(10);
        indices.Add(5);

        indices.Add(6);
        indices.Add(2);
        indices.Add(4);

        indices.Add(6);
        indices.Add(5);
        indices.Add(2);

        indices.Add(5);
        indices.Add(0);
        indices.Add(2);

        indices.Add(2);
        indices.Add(0);
        indices.Add(1);

        indices.Add(2);
        indices.Add(1);
        indices.Add(4);

        indices.Add(1);
        indices.Add(0);
        indices.Add(3);

        indices.Add(1);
        indices.Add(8);
        indices.Add(4);

        indices.Add(1);
        indices.Add(3);
        indices.Add(8);

        indices.Add(3);
        indices.Add(11);
        indices.Add(8);

        // Unfortunately, to give each vertex its own normal and texture coordinates (UVs), we
        // need to make each triangle have its own vertices instead of sharing them with neighboring triangles.

        // Duplicate vertices in the correct order for triangles to be indexed 0, 1, 2, 3...
        for (int i = 0; i < indices.Count; ++i)
        {
            verticesDuped.Add(vertices[indices[i]]);
        }

        // Reset our indices to the new ones 0, 1, 2, 3...
        indices.Clear();
        for (int i = 0; i < verticesDuped.Count; ++i)
        {
            indices.Add(i);
        }

        // Generate our own normals, perpendicular to the triangle surface
        List<Vector3> normals = new List<Vector3>();

        for(int i = 0; i < indices.Count; i += 3)
        {
            // The Cross product gives us a new vector perpendicular to the two we pass in.
            // If the two vectors are in the plane of the triangle, we get a normal to the triangle.
            Vector3 n = Vector3.Cross(verticesDuped[i] - verticesDuped[i + 1], verticesDuped[i] - verticesDuped[i + 2]).normalized;
            normals.Add(n);
            normals.Add(n);
            normals.Add(n);
        }

        mesh.vertices = verticesDuped.ToArray();
        mesh.triangles = indices.ToArray();
        mesh.normals = normals.ToArray();


        List<Vector2> uvs = new List<Vector2>();

        for(int i = 0; i < verticesDuped.Count; i += 3)
        {
            int n = i / 3;  // Which triangle I'm on

            int column = n%5;
            int row = n/5;

            // UVs are normalized (0, 1)
            float leftU = 0f + 1/5f*column;
            float topV = 1 - 1/4f*row;
            float rightU = 1/5f * (column + 1);
            float bottomV = 1 - 1/4f * (row + 1);

            uvs.Add(new Vector2(leftU, bottomV));
            uvs.Add(new Vector2((leftU + rightU)/2, topV));
            uvs.Add(new Vector2(rightU, bottomV));
        }

        mesh.uv = uvs.ToArray();

        //mesh.RecalculateNormals();  // No more!  We are in charge of the normals now.
        mesh.RecalculateTangents();



        MeshCollider mc = GetComponent<MeshCollider>();
        mc.sharedMesh = mesh;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
