﻿#define ROW_MAJOR

using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MatrixStuff : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        MakeIdentity();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    static Matrix4x4 MakeIdentity()
    {
        Matrix4x4 mat = new Matrix4x4();
        mat[0] = 1;
        mat[5] = 1;
        mat[10] = 1;
        mat[15] = 1;

        return mat;
    }

    static Matrix4x4 MakeTranslation(float x, float y, float z)
    {
        Matrix4x4 mat = new Matrix4x4();
        /*int[] arr = new int[16] {0, 1, 2, 3,
                                 4, 5, 6, 7,
                                 8, 9, 10, 11,
                                 12, 13, 14, 15};*/

        mat[0] = 1;
        mat[5] = 1;
        mat[10] = 1;
        mat[15] = 1;

#if ROW_MAJOR
        mat[3] = x;
        mat[7] = y;
        mat[11] = z;
#elif COLUMN_MAJOR
        mat[12] = x;
        mat[13] = y;
        mat[14] = z;
#endif

        return mat;
    }
}
