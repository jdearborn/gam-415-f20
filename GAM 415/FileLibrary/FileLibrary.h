#pragma once
#include <string>

#ifndef DLL_EXPORT
	#ifdef BUILD_LIBRARY
		#define DLL_EXPORT __declspec(dllexport)
	#else
		#define DLL_EXPORT __declspec(dllimport)
	#endif
#endif


void DLL_EXPORT WriteToFile(std::string filename, std::string content);


