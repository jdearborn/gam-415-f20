﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FunctionDatatypes_CS
{
    class OtherClass
    {
        public void PrintIt(int num)
        {
            Console.WriteLine("OtherClass: " + num);
        }
    }

    class Program
    {

        public delegate void ThingPrinter(int num);

        public static void PrintIt(int num)
        {
            Console.WriteLine("Program: " + num);
        }

        public static bool PrintFunc(int num)
        {
            if(num > 5)
            {
                Console.WriteLine("That is a big number!");
                return true;
            }
            else
            {
                Console.WriteLine("That is a small number... :(");
                return false;
            }
        }

        public static void PrintAction(int num)
        {
            if (num > 5)
            {
                Console.WriteLine("That is a big number!");
            }
            else
            {
                Console.WriteLine("That is a small number... :(");
            }
        }

        static void Main(string[] args)
        {
            ThingPrinter myFn = PrintIt;

            OtherClass myObj = new OtherClass();
            ThingPrinter myOtherFn = myObj.PrintIt;

            myFn(3);
            myOtherFn(7);


            // Func - Functions that return a value
            // Action - Functions that return void

            Func<int, bool> myFunc1 = PrintFunc;

            Action<int> myAction1 = PrintAction;


            myFunc1(17);
            myAction1(3);




            Console.ReadLine();
        }
    }
}
