#include <iostream>
#include <vector>
#include <functional>
using namespace std;


void PrintNumber(int num)
{
	cout << "Your number: " << num << endl;
}



class MyClass
{
public:
	void Update()
	{
		if (frameNumber % 10 == 0)
			OnTenthFrame();

		frameNumber++;
	}

	function<void()> OnTenthFrame;
private:
	int frameNumber = 1;
};




void PrintSomething()
{
	cout << "This something is being printed!" << endl;
}


int main(int argc, char* argv[])
{
	int myNum = 5;
	void (*myFnPtr)(int) = PrintNumber;

	myFnPtr(myNum);

	function<void(int)> myOtherFn = PrintNumber;
	myOtherFn(myNum);


	MyClass obj;

	obj.OnTenthFrame = PrintSomething;

	while (true)
	{
		obj.Update();
		cin.get();
	}



	cin.get();
	return 0;
}