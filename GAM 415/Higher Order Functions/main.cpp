#include <iostream>
#include <vector>
#include <functional>
using namespace std;

void ApplyToVector(vector<int> v, function<void(int)> f)
{
	for (size_t i = 0; i < v.size(); ++i)
	{
		f(v[i]);
	}
}

function<bool(int)> AdaptToReturnBool(function<void(int)> f)
{
	return [f](int a) { f(a); return true; };
}

void TestReturnBool(function<bool(int)> f)
{
	f(10);
}


void BadFn(int a)
{
	cout << "BadFn with " << a << endl;
}

int main(int argc, char* argv[])
{
	vector<int> v = {4, 1, 2, 5};

	ApplyToVector(v, [](int a) {cout << a << endl; });

	vector<int> other;
	ApplyToVector(v, [&other](int a) {other.push_back(a + 4); });


	ApplyToVector(other, [](int a) {cout << a << endl; });



	TestReturnBool(AdaptToReturnBool(BadFn));


	cin.get();
	return 0;
}