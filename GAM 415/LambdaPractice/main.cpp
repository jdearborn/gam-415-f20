#include <iostream>
#include <functional>
#include <conio.h>
using namespace std;

int main(int argc, char* argv[])
{
	int b[2][3] = {0, 1, 2,
				   3, 4, 5};

	cout << b[1][0] << endl;  // Second dimension is the "fast" one
	cout << b[0][2] << endl;

	// We usually would say that this order [m][n] is [row][column]
	// So for the best efficiency, we would increment the column.
	// We have to settle on a convention of how the matrix is stored.
	// Do we use column-vectors or row-vectors?


	std::function<void()> printTheThing = []() {};

	bool done = false;
	while (!done)
	{
		cout << "Please press a key (A, B, C, <space>)." << endl;
		char c = _getch();

		switch (c)
		{
		case 'A':
		case 'a':
			cout << "Assigned new function for A." << endl;
			printTheThing = []() {cout << "Lambda A" << endl; };
			break;
		case 'B':
		case 'b':
			printTheThing = []() {cout << "Lambda B" << endl; };
			break;
		case 'C':
		case 'c':
			printTheThing = []() {cout << "Lambda C" << endl; };
			break;
		case ' ':
			printTheThing();
			break;
		case 'Q':
		case 'q':
			//exit(0);
			done = true;
			// break 2;  // Does not exist
			// break MyWhile;  // Does not exist
			//goto EndMyWhile;  // This will make other programmers mad.
		default:
			break;
		}

	}

	//EndMyWhile:

	return 0;
}