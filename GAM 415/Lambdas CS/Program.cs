﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lambdas_CS
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> numbers = new List<int>();

            numbers.Add(32);
            numbers.Add(14);
            numbers.Add(1);
            numbers.Add(7);
            numbers.Add(9);
            numbers.Add(3);

            int index = numbers.FindIndex((int elem) => {return elem < 20;});

            Console.ReadLine();
        }
    }
}
