#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>
using namespace std;

void AddOne(int& num)
{
	num += 1;
}

void AddOneToVector(vector<int>& v)
{
	for (int i = 0; i < v.size(); ++i)
	{
		v[i] += 1;
	}
}

// Higher-order function
void ApplyToVector(std::function<void(int&)> fn, vector<int>& v)
{
	for (int i = 0; i < v.size(); ++i)
	{
		fn(v[i]);
	}
}

int main(int argc, char* argv[])
{
	int number = 5;
	AddOne(number);


	// Anonymous functions (lambdas)
	// [capture](arguments/parameters) {body}

	//[](int& num) {num += 1; } (number);
	auto addOne = [](int& num) {
		num += 1;
	};

	addOne(number);


	vector<int> v = {21, 14, 1, 10, 77, 9};

	for (int i = 0; i < v.size(); ++i)
	{
		addOne(v[i]);
	}


	ApplyToVector([](int& num) {num *= 2; }, v);

	auto e = std::find_if(v.begin(), v.end(), [](int num) {return num < 20; });
	if (e != v.end())
	{
		cout << "Found it! " << *e << endl;
	}




	cin.get();
	return 0;
}