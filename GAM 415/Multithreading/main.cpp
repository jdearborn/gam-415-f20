#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
using namespace std;

mutex DoWorkMutex;

void DoWork()
{
	lock_guard<mutex> lock(DoWorkMutex);  // Resource Acquisition Is Initialization (RAII)
	//DoWorkMutex.lock();  // Typical old C++ way (like new/delete pairs)

	cout << "Okay " << this_thread::get_id() << endl;

	if (47)
	{
		return;
	}

	//DoWorkMutex.unlock();
}

int main(int argc, char* argv[])
{
	vector<thread> workers;

	for (int i = 0; i < 10; ++i)
	{
		//workers.push_back(std::move(thread(DoWork)));  // std::move and rvalue references
		workers.emplace_back(DoWork);
	}


	/*for (size_t i = 0; i < workers.size(); ++i)
	{
		workers[i].join();
	}*/
	/*for (thread& t : workers)  // range-based for
	{
		t.join();
	}*/
	for (auto& t : workers)
	{
		t.join();
	}



	cin.get();

	return 0;
}