#include <iostream>
#include <thread>
#include <mutex>
#include <list>
#include <vector>
#include <memory>
using namespace std;


enum class TaskType
{
	NONE,
	INCREMENT_COUNTER,
	STORE_COUNTER,
	STORE_INT,
	PRINT_STRING
};

class TaskDataBase
{
public:
	TaskType task;

	TaskDataBase()
		: task(TaskType::NONE)
	{}

	TaskDataBase(TaskType taskType)
		: task(taskType)
	{}

	virtual ~TaskDataBase()
	{}
};

template<typename T>
class TaskData : public TaskDataBase
{
public:
	T data;

	TaskData(TaskType taskType, const T& data)
		: TaskDataBase(taskType), data(data)
	{}

	virtual ~TaskData() override
	{}
};

// Serial queue of work to be done
list<shared_ptr<TaskDataBase>> taskQueue;


// Task data storage
int counter = 0;
vector<int> storage;


// Task actions
void IncrementCounter()
{
	++counter;
}

void StoreInt(int i)
{
	storage.push_back(i);
}

void PrintString(string s)
{
	cout << s << endl;
}



int main(int argc, char* argv[])
{

	// Set up some work to be done
	for (int i = 0; i < 200; ++i)
	{
		switch (rand() % 4)
		{
		case 0:
			taskQueue.push_back(make_shared<TaskDataBase>(TaskType::INCREMENT_COUNTER));
			break;
		case 1:
			taskQueue.push_back(make_shared<TaskDataBase>(TaskType::STORE_COUNTER));
			break;
		case 2:
			taskQueue.push_back(make_shared<TaskData<int>>(TaskType::STORE_INT, 1000 + rand()%101));
			break;
		case 3:
			taskQueue.push_back(make_shared<TaskData<string>>(TaskType::PRINT_STRING, "Hey"));
			break;
		}
	}


	// Do the work
	cout << "Processing " << taskQueue.size() << " tasks." << endl;
	while(taskQueue.size() > 0)
	{
		auto ptr = taskQueue.front();
		switch (ptr->task)
		{
		case TaskType::INCREMENT_COUNTER:
			IncrementCounter();
			break;
		case TaskType::STORE_COUNTER:
			StoreInt(counter);
			break;
		case TaskType::STORE_INT:
			StoreInt(static_pointer_cast<TaskData<int>>(ptr)->data);
			break;
		case TaskType::PRINT_STRING:
			PrintString(static_pointer_cast<TaskData<string>>(ptr)->data);
			break;
		default:
			break;
		}
		taskQueue.pop_front();
	}




	// Let's check out the results...


	cout << "Counter: " << counter << endl;

	// Print out all of storage
	cout << "Storage: " << endl;
	for (auto i : storage)
	{
		cout << i << " ";
	}
	cout << endl;


	cin.get();

	return 0;
}