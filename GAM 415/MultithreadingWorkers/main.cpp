#include <iostream>
#include <thread>
#include <mutex>
#include <vector>
#include <chrono>
using namespace std;
using namespace chrono;

// Goal: Sum all of these integers as fast as possible.
int sum = 0;
vector<int> values;
// O(n)
// O(n) / N


mutex DoWorkMutex;

void DoWork()
{
	lock_guard<mutex> lock(DoWorkMutex);  // Resource Acquisition Is Initialization (RAII)


}

void DoSingleThread()
{
	// Serially, on one thread: ~4,000,000 ns
	high_resolution_clock::time_point start = high_resolution_clock::now();
	for (size_t i = 0; i < values.size(); ++i)
	{
		sum += values[i];
	}
	high_resolution_clock::time_point finish = high_resolution_clock::now();

	nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);

	cout << "Sum: " << sum << endl;
	cout << "(Single) That took " << totalNS.count() / 1000000000.0 << " seconds!" << endl;
}

// endIndex is exclusive (we won't actually read that position in the vector)
int SumRange(size_t startIndex, size_t endIndex)
{
	int result = 0;

	for (size_t i = startIndex; i < endIndex; ++i)
	{
		result += values[i];
	}

	return result;
}

void DoMultiThread(int N)
{
	high_resolution_clock::time_point start = high_resolution_clock::now();

	int* results = new int[N];
	vector<thread> workers;

	int rangeWidth = values.size() / N;
	int i;
	for (i = 0; i < N-1; ++i)
	{
		workers.emplace_back([&results, N, i, rangeWidth]() {
			results[i] = SumRange(i* rangeWidth, (i+1)* rangeWidth);
			});
	}

	workers.emplace_back([&results, N, i, rangeWidth]() {
		results[i] = SumRange(i * rangeWidth, values.size());
		});



	for (auto& t : workers)
	{
		t.join();
	}

	for (int i = 0; i < N; ++i)
	{
		sum += results[i];
	}
	delete[] results;

	high_resolution_clock::time_point finish = high_resolution_clock::now();

	nanoseconds totalNS = duration_cast<nanoseconds>(finish - start);

	cout << "Sum: " << sum << endl;
	cout << "(Multi) That took " << totalNS.count()/1000000000.0 << " seconds!" << endl;

}


int main(int argc, char* argv[])
{
	// Set up our data
	for (int i = 0; i < 10000000; ++i)
	{
		values.push_back(1);
	}

	cout << "Starting timer..." << endl;

	//DoSingleThread();
	DoMultiThread(8);


	cin.get();

	return 0;
}