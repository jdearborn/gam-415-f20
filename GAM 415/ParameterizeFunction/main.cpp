#include <iostream>
#include <string>
#include <fstream>
#include <functional>
using namespace std;

// No parameters (function arguments)!
void MyFn()
{
	cout << "Hello!" << endl;
}

void MyFn2(string str)
{
	for (int i = 0; i < 5; ++i)
	{
		cout << str << endl;
	}
}

void MyFn3(string str, int count)
{
	for (int i = 0; i < count; ++i)
	{
		cout << str << endl;
	}
}

void MyFn4(string str, int count, ofstream& fout)
{
	for (int i = 0; i < count; ++i)
	{
		fout << str << endl;
	}
}

void MyFn5(string str, int count, function<void(string)> f)
{
	for (int i = 0; i < count; ++i)
	{
		f(str);
	}
}

int main(int argc, char* argv[])
{
	MyFn();
	MyFn2("Hello!");
	MyFn3("Hello!", 1);
	MyFn3("Hello!", 5);

	ofstream fout;
	fout.open("myfile.txt");
	MyFn4("Hello!", 5, fout);
	fout.close();

	MyFn5("Hello!", 5, [](string a) {cout << a << endl; });

	return 0;
}