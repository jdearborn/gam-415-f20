﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Planet : MonoBehaviour
{
    private LineRenderer lr;
    private List<Vector3> positions = new List<Vector3>();

    public float mass = 1e20f;
    public Vector3 velocity = Vector3.zero;
    public Vector3 acceleration = Vector3.zero;


    // Start is called before the first frame update
    void Start()
    {
        PlanetCollector.Instance.Add(this);
        lr = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Random.Range(0, 10) == 0)
        {
            positions.Add(transform.position);

            lr.positionCount = positions.Count;
            lr.SetPositions(positions.ToArray());
        }
    }

    private void FixedUpdate()
    {
        List<Planet> all = PlanetCollector.Instance.GetAll();

        for(int i = 0; i < all.Count; ++i)
        {
            Planet other = all[i];
            if (other == this)
                continue;

            float distance = Vector3.Distance(transform.position, other.transform.position);
            Vector3 direction = (other.transform.position - transform.position).normalized;

            // Newton's Law of Universal Gravitation
            // F = G m*M / r^2
            float G = 6.6743e-11f;

            // Newton's Second Law
            // F = ma
            //Vector3 force = direction * G * mass * other.mass / (distance*distance);
            //acceleration += force/mass;

            acceleration += direction * G * other.mass / (distance*distance);
        }

        // Forward (explicit) euler
        // x_new = x_old * t;

        /*transform.position += velocity * Time.fixedDeltaTime;
        velocity += acceleration * Time.fixedDeltaTime;*/

        // Semi-implicit (simplectic) euler
        velocity += acceleration * Time.fixedDeltaTime;
        transform.position += velocity * Time.fixedDeltaTime;


        // An implicit method
        //x_new = (x_old + x_new)/2 * t;

        acceleration = Vector3.zero;
    }
}
