﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Singleton to store all planet references
public class PlanetCollector : MonoBehaviour
{
    private static PlanetCollector _instance = null;
    public static PlanetCollector Instance { get { return _instance; }
        set {
            if (_instance == null)
            {
                _instance = value;
                DontDestroyOnLoad(value);
            }
            else
            {
                Destroy(value.gameObject);
            }
        }
    }

    private List<Planet> planets = new List<Planet>();

    void Awake()
    {
        Instance = this;
    }

    public void Add(Planet p)
    {
        planets.Add(p);
    }

    public List<Planet> GetAll()
    {
        return planets;
    }
}
