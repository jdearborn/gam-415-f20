#include <iostream>
#include <string>
#include "SimpleGraphics.h"
using namespace std;



class Sprite
{
public:
	float x, y;
	float degrees;
	Image image;

	Sprite(const std::string& filename)
		: image(filename)
	{
		x = 0.0f;
		y = 0.0f;
		degrees = 0.0f;
	}

	void Draw()
	{
		image.DrawToScreen(x, y, degrees);
	}
};



void DrawGradient()
{
	int numRows = 100;
	int numColumns = 100;
	for (int row = 0; row < numRows; ++row)
	{
		for (int column = 0; column < numColumns; ++column)
		{
			uint8_t red = (float(row) / numRows) * 255;
			uint8_t green = 255;
			uint8_t blue = 255;

			SG_SetPixel(column, row, red, green, blue);
		}
	}
}

void FillImageWithGradient(Image& image)
{
	image.SetAsTarget();

	for (int row = 0; row < image.GetHeight(); ++row)
	{
		for (int column = 0; column < image.GetWidth(); ++column)
		{
			uint8_t red = (float(row) / image.GetHeight()) * 255;
			uint8_t green = 255;
			uint8_t blue = 255;

			image.SetPixel(column, row, red, green, blue);
		}
	}

	image.UnsetAsTarget();
}

void StartRendering(int width, int height, int pixelDelay, std::function<void(float)> renderCallback)
{
	SG_Initialize("Image test", width, height);

	SG_SetPixelDelay(pixelDelay);

	SG_Run(renderCallback);

	SG_CleanUp();
}

int main(int argc, char* argv[])
{
	SG_Initialize("Image test", 800, 600);

	SG_SetPixelDelay(1);


	Sprite smile("smile.bmp");

	SG_Run([&smile](float dt)
	{
		//FillImageWithGradient(smile.image);
		//smile.Draw();
		DrawGradient();

		SG_SetPixel(100, 50, 255, 0, 0);
	});

	smile.image.Free();

	SG_CleanUp();

	return 0;
}